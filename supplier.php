<?php  
    if (isset($_GET['hapus'])) {
        $supplier->hapus_supplier($_GET['hapus']);
        echo "<script>location='index.php?page=supplier';</script>";
    }
?>
<div class="row">
    <div class="col-md-12">
    <!-- Advanced Tables -->
        <div class="panel panel-default">
            <div class="panel-heading">
                Data Pelangan
            </div>
            <div class="panel-body">
                <div class="table">
                    <table class="table table-striped table-bordered table-hover" id="tabelku">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Alamat</th>
								<th>No Telp</th>
								<th>Jenis Motor</th>
								<th>Nomor Plat Motor</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php  
                                $sp = $supplier->tampil_supplier();
                                foreach ($sp as $index => $data) {
                            ?>
                            <tr class="odd gradeX">
                                <td><?php echo $index + 1; ?></td>
                                <td><?php echo $data['nama']; ?></td>
                                <td><?php echo $data['alamat']; ?></td>
								<td><?php echo $data['no_telepon']; ?></td>
								<td><?php echo $data['jenis_motor']; ?></td>
								<td><?php echo $data['nomor_plat']; ?></td>
                                <td>
                                    <a href="index.php?page=ubahsupplier&id=<?php echo $data['kd_supplier']; ?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit</a>
                                    <a href="index.php?page=supplier&hapus=<?php echo $data['kd_supplier']; ?>" class="btn btn-danger btn-xs" id="alertHapus"><i class="fa fa-trash"></i> Hapus</a>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>   
            </div>
            <div class="panel-footer">
                <a href="index.php?page=tambahsupplier" class="btn btn-success"><i class="fa fa-plus"></i> Tambah Pelangan</a>
            </div>
        </div>
        <!--End Advanced Tables -->
    </div>
</div>