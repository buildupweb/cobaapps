-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 20 Jul 2018 pada 03.36
-- Versi Server: 5.5.39
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `inventory_barang`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `kd_admin` int(6) NOT NULL,
  `nama` varchar(20) NOT NULL,
  `email` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `gambar` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`kd_admin`, `nama`, `email`, `password`, `gambar`) VALUES
(5, 'Jan', 'jantrius@admin.com', 'jantrius', 'LOGO.png'),
(6, 'Admin', 'admin@admin.com', 'admin', 'LOGO.png'),
(7, 'Raskita YS Hasugian', 'raskita@admin.com', 'raskitaysh', 'LOGO.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang`
--

CREATE TABLE IF NOT EXISTS `barang` (
  `kd_barang` varchar(8) NOT NULL,
  `nama_barang` varchar(20) NOT NULL,
  `satuan` varchar(6) NOT NULL,
  `harga_jual` int(15) NOT NULL,
  `harga_beli` int(15) NOT NULL,
  `stok` int(4) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `barang`
--

INSERT INTO `barang` (`kd_barang`, `nama_barang`, `satuan`, `harga_jual`, `harga_beli`, `stok`, `status`) VALUES
('BB000001', 'Oli Yamalube Silver', 'PCS', 60000, 55000, 11, '0'),
('BB000002', 'Oli Yamalube Sport', 'PCS', 60000, 55000, 12, '0'),
('BB000003', 'Oli Yamalube Matic', 'PCS', 50000, 45000, 12, '0'),
('BB000004', 'Oli Top One Matic', 'PCS', 50000, 45000, 12, '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `barangp_sementara`
--

CREATE TABLE IF NOT EXISTS `barangp_sementara` (
  `id_barangp` int(6) NOT NULL,
  `kd_pembelian` char(8) NOT NULL,
  `nama_pelangan` varchar(20) NOT NULL,
  `identitas_motor` varchar(20) NOT NULL,
  `jasa_service` varchar(20) NOT NULL,
  `nama_barangp` varchar(225) NOT NULL,
  `satuan` varchar(20) NOT NULL,
  `harga_barangp` double NOT NULL,
  `item` int(4) NOT NULL,
  `total` double NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data untuk tabel `barangp_sementara`
--

INSERT INTO `barangp_sementara` (`id_barangp`, `kd_pembelian`, `nama_pelangan`, `identitas_motor`, `jasa_service`, `nama_barangp`, `satuan`, `harga_barangp`, `item`, `total`) VALUES
(4, 'PEM00002', 'Andante', 'Suzuki Satria', 'Service Ringan', 'Oli Yamalube Sport', 'PCS', 60000, 1, 60000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang_pembelian`
--

CREATE TABLE IF NOT EXISTS `barang_pembelian` (
  `kd_permintaanbarang` varchar(8) NOT NULL,
  `kd_barang_beli` int(6) NOT NULL,
  `nama_barang_beli` varchar(20) NOT NULL,
  `satuan` varchar(6) NOT NULL,
  `harga_beli` double NOT NULL,
  `item` int(4) NOT NULL,
  `total` double NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=108 ;

--
-- Dumping data untuk tabel `barang_pembelian`
--

INSERT INTO `barang_pembelian` (`kd_permintaanbarang`, `kd_barang_beli`, `nama_barang_beli`, `satuan`, `harga_beli`, `item`, `total`, `status`) VALUES
('PEM00001', 106, 'Oli Yamalube Matic', 'PCS', 50000, 12, 600000, '1'),
('PEM00001', 107, 'Oli Yamalube Matic', 'PCS', 50000, 12, 600000, '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `d_pembelian`
--

CREATE TABLE IF NOT EXISTS `d_pembelian` (
  `id_pembelian` int(6) NOT NULL,
  `kd_pembelian` char(8) NOT NULL,
  `kd_barang_beli` int(6) NOT NULL,
  `jumlah` int(6) NOT NULL,
  `subtotal` double NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=161 ;

--
-- Dumping data untuk tabel `d_pembelian`
--

INSERT INTO `d_pembelian` (`id_pembelian`, `kd_pembelian`, `kd_barang_beli`, `jumlah`, `subtotal`) VALUES
(160, 'PEM00001', 107, 12, 600000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `d_penjualan`
--

CREATE TABLE IF NOT EXISTS `d_penjualan` (
  `id_penjualan` int(6) NOT NULL,
  `kd_penjualan` char(8) NOT NULL,
  `kd_barang` varchar(8) NOT NULL,
  `jumlah` int(4) NOT NULL,
  `subtotal` double NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=57 ;

--
-- Dumping data untuk tabel `d_penjualan`
--

INSERT INTO `d_penjualan` (`id_penjualan`, `kd_penjualan`, `kd_barang`, `jumlah`, `subtotal`) VALUES
(55, 'PEN00001', 'BB000003', 1, 100000),
(56, 'PEN00002', 'BB000001', 1, 60000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembelian`
--

CREATE TABLE IF NOT EXISTS `pembelian` (
  `kd_pembelian` char(8) NOT NULL,
  `tgl_workorder` date NOT NULL,
  `kd_supplier` int(6) NOT NULL,
  `nama_pelangan` varchar(20) NOT NULL,
  `identitas_motor` varchar(20) NOT NULL,
  `jasa_service` varchar(20) NOT NULL,
  `nama_barang` varchar(20) NOT NULL,
  `total` double NOT NULL,
  `kd_admin` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pembelian`
--

INSERT INTO `pembelian` (`kd_pembelian`, `tgl_workorder`, `kd_supplier`, `nama_pelangan`, `identitas_motor`, `jasa_service`, `nama_barang`, `total`, `kd_admin`) VALUES
('PEM00001', '2016-03-13', 4, 'Yunia', 'Honda Beat B333IND', 'Service Ringan', 'Oli Yamalube Matic', 100000, 6);

-- --------------------------------------------------------

--
-- Struktur dari tabel `penjualan`
--

CREATE TABLE IF NOT EXISTS `penjualan` (
  `kd_penjualan` char(8) NOT NULL,
  `tgl_penjualan` date NOT NULL,
  `kd_barang` varchar(8) NOT NULL,
  `nama_barang` varchar(20) NOT NULL,
  `satuan` varchar(6) NOT NULL,
  `item` int(4) NOT NULL,
  `dibayar` double NOT NULL,
  `total_penjualan` double NOT NULL,
  `kd_admin` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `penjualan`
--

INSERT INTO `penjualan` (`kd_penjualan`, `tgl_penjualan`, `kd_barang`, `nama_barang`, `satuan`, `item`, `dibayar`, `total_penjualan`, `kd_admin`) VALUES
('PEN00001', '2016-03-13', 'BB000003', 'Oli Yamalube Matic', 'PCS', 1, 100000, 100000, 6),
('PEN00002', '2018-06-01', 'BB000001', 'Oli Yamalube Silver', 'PCS', 1, 100000, 60000, 6);

-- --------------------------------------------------------

--
-- Struktur dari tabel `penjualan_sementara`
--

CREATE TABLE IF NOT EXISTS `penjualan_sementara` (
  `id_penjualan_sementara` int(11) NOT NULL,
  `kd_penjualan` char(8) NOT NULL,
  `kd_barang` varchar(8) NOT NULL,
  `nama_barang` varchar(225) NOT NULL,
  `satuan` varchar(30) NOT NULL,
  `harga` double NOT NULL,
  `item` int(4) NOT NULL,
  `total` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `perusahaan`
--

CREATE TABLE IF NOT EXISTS `perusahaan` (
  `kd_perusahaan` int(11) NOT NULL,
  `nama_perusahaan` varchar(20) NOT NULL,
  `alamat` varchar(30) NOT NULL,
  `pemilik` varchar(20) NOT NULL,
  `kota` varchar(20) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `perusahaan`
--

INSERT INTO `perusahaan` (`kd_perusahaan`, `nama_perusahaan`, `alamat`, `pemilik`, `kota`) VALUES
(1, 'JALOK''N MOTOR', 'Jln. Iskandar Muda No 11 RT002', 'Syamsui Hasugian', 'Tangerang');

-- --------------------------------------------------------

--
-- Struktur dari tabel `supplier`
--

CREATE TABLE IF NOT EXISTS `supplier` (
  `kd_supplier` int(6) NOT NULL,
  `nama` varchar(20) NOT NULL,
  `alamat` varchar(20) NOT NULL,
  `no_telepon` int(15) NOT NULL,
  `jenis_motor` varchar(15) NOT NULL,
  `nomor_plat` varchar(15) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `supplier`
--

INSERT INTO `supplier` (`kd_supplier`, `nama`, `alamat`, `no_telepon`, `jenis_motor`, `nomor_plat`) VALUES
(1, 'Rita', 'AMD', 123456789, 'Yamaha Vario', 'B111IND'),
(3, 'Andante', 'Perumahaan Bandara', 1234567890, 'Suzuki Satria', 'B222IND'),
(4, 'Yunia', 'Sewan', 215501086, 'Honda Beat', 'B333IND');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
 ADD PRIMARY KEY (`kd_admin`);

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
 ADD PRIMARY KEY (`kd_barang`);

--
-- Indexes for table `barangp_sementara`
--
ALTER TABLE `barangp_sementara`
 ADD PRIMARY KEY (`id_barangp`), ADD KEY `kd_pembelian` (`kd_pembelian`);

--
-- Indexes for table `barang_pembelian`
--
ALTER TABLE `barang_pembelian`
 ADD PRIMARY KEY (`kd_barang_beli`);

--
-- Indexes for table `d_pembelian`
--
ALTER TABLE `d_pembelian`
 ADD PRIMARY KEY (`id_pembelian`), ADD KEY `kd_pembelian` (`kd_pembelian`), ADD KEY `kd_pembelian_2` (`kd_pembelian`), ADD KEY `kd_barang_beli` (`kd_barang_beli`), ADD KEY `kd_barang_beli_2` (`kd_barang_beli`);

--
-- Indexes for table `d_penjualan`
--
ALTER TABLE `d_penjualan`
 ADD PRIMARY KEY (`id_penjualan`), ADD KEY `kd_penjualan` (`kd_penjualan`), ADD KEY `kd_barang` (`kd_barang`), ADD KEY `kd_barang_2` (`kd_barang`);

--
-- Indexes for table `pembelian`
--
ALTER TABLE `pembelian`
 ADD PRIMARY KEY (`kd_pembelian`), ADD KEY `kd_admin` (`kd_admin`), ADD KEY `kd_supplier` (`kd_supplier`);

--
-- Indexes for table `penjualan`
--
ALTER TABLE `penjualan`
 ADD PRIMARY KEY (`kd_penjualan`), ADD KEY `kd_admin` (`kd_admin`);

--
-- Indexes for table `penjualan_sementara`
--
ALTER TABLE `penjualan_sementara`
 ADD PRIMARY KEY (`id_penjualan_sementara`);

--
-- Indexes for table `perusahaan`
--
ALTER TABLE `perusahaan`
 ADD PRIMARY KEY (`kd_perusahaan`), ADD KEY `kd_perusahaan` (`kd_perusahaan`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
 ADD PRIMARY KEY (`kd_supplier`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
MODIFY `kd_admin` int(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `barangp_sementara`
--
ALTER TABLE `barangp_sementara`
MODIFY `id_barangp` int(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `barang_pembelian`
--
ALTER TABLE `barang_pembelian`
MODIFY `kd_barang_beli` int(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=108;
--
-- AUTO_INCREMENT for table `d_pembelian`
--
ALTER TABLE `d_pembelian`
MODIFY `id_pembelian` int(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=161;
--
-- AUTO_INCREMENT for table `d_penjualan`
--
ALTER TABLE `d_penjualan`
MODIFY `id_penjualan` int(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `penjualan_sementara`
--
ALTER TABLE `penjualan_sementara`
MODIFY `id_penjualan_sementara` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `perusahaan`
--
ALTER TABLE `perusahaan`
MODIFY `kd_perusahaan` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
MODIFY `kd_supplier` int(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `d_pembelian`
--
ALTER TABLE `d_pembelian`
ADD CONSTRAINT `d_pembelian_ibfk_3` FOREIGN KEY (`kd_pembelian`) REFERENCES `pembelian` (`kd_pembelian`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `d_pembelian_ibfk_4` FOREIGN KEY (`kd_barang_beli`) REFERENCES `barang_pembelian` (`kd_barang_beli`);

--
-- Ketidakleluasaan untuk tabel `d_penjualan`
--
ALTER TABLE `d_penjualan`
ADD CONSTRAINT `d_penjualan_ibfk_3` FOREIGN KEY (`kd_barang`) REFERENCES `barang` (`kd_barang`),
ADD CONSTRAINT `d_penjualan_ibfk_4` FOREIGN KEY (`kd_penjualan`) REFERENCES `penjualan` (`kd_penjualan`);

--
-- Ketidakleluasaan untuk tabel `pembelian`
--
ALTER TABLE `pembelian`
ADD CONSTRAINT `pembelian_ibfk_1` FOREIGN KEY (`kd_supplier`) REFERENCES `supplier` (`kd_supplier`),
ADD CONSTRAINT `pembelian_ibfk_2` FOREIGN KEY (`kd_admin`) REFERENCES `admin` (`kd_admin`);

--
-- Ketidakleluasaan untuk tabel `penjualan`
--
ALTER TABLE `penjualan`
ADD CONSTRAINT `penjualan_ibfk_1` FOREIGN KEY (`kd_admin`) REFERENCES `admin` (`kd_admin`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
