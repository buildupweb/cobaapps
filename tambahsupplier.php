<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title" style="padding-top:0; margin-top:0; color:#f00;">Tambah Pelangan</h3>
			</div>
			<hr/>
			<div class="box-body">	
				<?php 
					if (isset($_POST['save'])) {
						$supplier->simpan_supplier($_POST['nama'],$_POST['alamat']);
						echo "<div class='alert alert-info alert-dismissable' id='divAlert'>
                                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                                Data Tersimpan
                                </div>";
					}
				?>	
				<form method="POST" id="forminput" enctype="multipart/form-data">
					<div class="form-group">
						<label>Nama Pelangan</label>
						<input type="text" class="form-control" name="nama" id="nama" placeholder="Masukan Nama Pelangan">
					</div>
					<div class="form-group">
						<label>Alamat</label>
						<input type="text" class="form-control" name="alamat" id="alamat" placeholder="Masukan Alamat Pelangan">
					</div>
					<div class="form-group">
						<label>No Telp</label>
						<input type="text" class="form-control" name="no_telepon" id="no_telepon" placeholder="Masukan No Telp Pelangan">
					</div>
					<div class="form-group">
						<label>Jenis Motor</label>
						<input type="text" class="form-control" name="jenis_motor" id="jenis_motor" placeholder="Masukan Jenis Motor Pelangan">
					</div>
					<div class="form-group">
						<label>Nomor Plat Motor</label>
						<input type="text" class="form-control" name="nomor_plat" id="nomor_plat" placeholder="Masukan Nomor Plat Motor Pelangan">
					</div>
					<button id="formbtn" class="btn btn-primary" name="save"><i class="fa fa-save"></i> Simpan</button>
					<a href="index.php?page=supplier" class="btn btn-warning"><i class="fa fa-arrow-left"></i> Back to supplier</a>
				</form>
			</div>
		</div>
	</div>
</div>
<script>
	//fungsi hide div
	$(function(){
		setTimeout(function(){$("#divAlert").fadeOut(900)}, 500);
	});
	function validateText(id){
		if ($('#'+id).val()== null || $('#'+id).val()== "") {
			var div = $('#'+id).closest('div');
			div.addClass("has-error has-feedback");
			return false;
		}
		else{
			var div = $('#'+id).closest('div');
			div.removeClass("has-error has-feedback");
			return true;	
		}
	}
	$(document).ready(function(){
		$("#formbtn").click(function(){
			if (!validateText('nama')) {
				$('#nama').focus();
				return false;
			}
			if (!validateText('alamat')) {
				$('#alamat').focus();
				return false;
			}
			return true;
		});
	});
</script>
