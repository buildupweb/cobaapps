<?php  
    if (isset($_GET['hapus'])) {
        $pembelian->hapus_pembelian($_GET['hapus']);
        echo "<script>location='index.php?page=pembelian';</script>";
    }

?>
<div class="row">
    <div class="col-md-12">
        <!-- Advanced Tables -->
        <div class="panel panel-default">
            <div class="panel-heading">
                Work Order
            </div>
            <div class="panel-body">
                <div class="table">
                    <table class="table table-striped table-bordered table-hover" id="tabelku">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kd WO</th>
                                <th>Tgl WO</th>
								<th>Kd Pelangan</th>
                                <th>Nama Pelangan</th>
                                <th>Identitas Motor</th>
                                <th>Jasa Service</th>
								<th>Nama Sparepart</th>
                                <th>Total</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php  
                                $pem = $pembelian->tampil_pembelian();
                                foreach ($pem as $index => $data) {
                                    $jumlahb = $pembelian->hitung_item_pembelian($data['kd_pembelian']);
                            ?>
                            <tr class="odd gradeX">
                                <td><?php echo $index + 1; ?></td>
                                <td><?php echo $data['kd_pembelian']; ?></td>
                                <td><?php echo $data['tgl_workorder']; ?></td>
								<td><?php echo $data['kd_supplier']; ?></td>
                                <td><?php echo $data['nama_pelangan']; ?></td>
                                <td><?php echo $data['identitas_motor']; ?></td>
								<td><?php echo $data['jasa_service']; ?></td>
								<td><?php echo $data['nama_barang']; ?></td>
                                <td>Rp. <?php echo number_format($data['total']); ?></td>
                                <td>
                                    <a href="nota/cetakdetailpembelian.php?kdpembelian=<?php echo $data['kd_pembelian']; ?>" target="_BLANK" class="btn btn-info btn-xs"><i class="fa fa-search"></i> Detail</a>
                                    
                                    <?php 
                                    $cek = $pembelian->cek_hapuspembelian($data['kd_pembelian']);
                                    if ($cek === true): ?>
                                    <a href="index.php?page=pembelian&hapus=<?php echo $data['kd_pembelian']; ?>" class="btn btn-danger btn-xs" id="alertHapus"><i class="fa fa-trash"></i> Hapus</a>                                        
                                    <?php endif ?>
                                    <?php if ($cek === false): ?>
                                    <a href="index.php?page=pembelian&hapus=<?php echo $data['kd_pembelian']; ?>" class="btn btn-danger btn-xs" id="alertHapus" disabled="disabled"><i class="fa fa-trash"></i> Hapus</a>
                                    <?php endif ?>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>   
            </div>
        </div>
        <!--End Advanced Tables -->
    </div>
</div>